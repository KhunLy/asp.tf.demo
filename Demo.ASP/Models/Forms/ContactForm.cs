﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.ASP.Models.Forms
{
    public class ContactForm
    {
        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Subject { get; set; }
        
        
        [Required]
        [MaxLength(1000)]
        public string Content { get; set; }
    }
}
