﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.ASP.Models.Forms
{
    public class TableForm
    {
        public int NbTables { get; set; }
        public int StartTable { get; set; }
    }
}
