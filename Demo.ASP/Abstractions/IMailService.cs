﻿namespace Demo.ASP.Abstractions
{
    public interface IMailService
    {
        bool SendEmail(string subject, string content, params string[] dests);
    }
}