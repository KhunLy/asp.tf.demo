﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Threading.Tasks;
using Demo.ASP.Abstractions;

namespace Demo.ASP.Services
{
    public class MailService : IMailService
    {
        private MailConfig _config;

        public MailService(MailConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// Envoyer un email
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="dests"></param>
        /// <returns></returns>
        public bool SendEmail(string subject, string content, params string[] dests)
        {
            using (SmtpClient client = new SmtpClient())
            {
                client.Host = _config.Host;
                client.Port = _config.Port;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(_config.Email, _config.Password);
                using (MailMessage message = new MailMessage())
                {
                    message.From = new MailAddress(_config.Email);
                    message.Subject = subject;
                    message.Body = content;
                    message.IsBodyHtml = true;
                    foreach (string dest in dests)
                    {
                        message.To.Add(dest);
                    }
                    try
                    {
                        client.Send(message);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }
    }
}
